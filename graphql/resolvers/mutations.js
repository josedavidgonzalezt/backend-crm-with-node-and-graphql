const User = require("../../models/User")
const bcryptjs = require("bcryptjs")
const Product = require("../../models/Product")
const Client = require("../../models/Client")
const Order = require("../../models/Order")

const createNewUser = async (_, { new_user }) => {
  const { email, password } = new_user

  // revisar si esta registrado
  const findUser = await User.findOne({ email })

  if (findUser) throw new Error("El User ya esta registrado")

  // hashear password
  const salt = await bcryptjs.genSalt(10)
  const passwordHash = await bcryptjs.hash(password, salt)

  //guardar en la DB
  try {
    const createNewUser = new User({ ...new_user, password: passwordHash })
    await createNewUser.save()
    return createNewUser
  } catch (error) {
    console.log(error)
  }
}

const createNewProduct = async (_, { product }) => {
  try {
    const newProduct = new Product(product)
    await newProduct.save()
    return newProduct
  } catch (error) {
    console.log(error)
  }
}

const updateProduct = async (_, { product_id, product }) => {
  let productToEdit = await Product.findById(product_id)

  if (!productToEdit) throw new Error("No existe el Product a Editar")

  productToEdit = await Product.findOneAndUpdate({ _id: product_id }, product, {
    new: true,
  })

  return productToEdit
}

const deleteProduct = async (_, { product_id }) => {
  let productToDelete = await Product.findById(product_id)

  if (!productToDelete) throw new Error("No existe el Product a Eliminar")

  await Product.findOneAndDelete({ _id: product_id })

  return "Product Eliminado"
}

const newClient = async (_, { new_client }, ctx) => {
  const { email } = new_client

  const findClient = await Client.findOne({ email })

  if (findClient) throw new Error("Este cliente ya existe")
  const newClient = new Client(new_client)

  newClient.seller = ctx.user.id

  try {
    await newClient.save()
    return newClient
  } catch (error) {
    console.log(error)
  }
}

const updateClient = async (_, { client_id, client }, ctx) => {
  const clientToEdit = await Client.findById(client_id)

  if (!clientToEdit) throw new Error("No existe el cliente a Editar")
  if (!ctx.user || clientToEdit.seller.toString() !== ctx.user.id.toString())
    throw new Error("Usted no puede hacer modificaciones en este cliente")

  const clientEdited = await Client.findOneAndUpdate(
    { _id: client_id },
    client,
    { new: true }
  )

  return {
    oldClient: clientToEdit,
    editedClient: clientEdited,
  }
}

const deleteClient = async (_, { client_id }, ctx) => {
  let findClientToDelet = await Client.findById(client_id)
  if (!findClientToDelet)
    throw new Error("El cliente que desea eliminar no existe")
  if (
    !ctx.user ||
    findClientToDelet.seller.toString() !== ctx.user.id.toString()
  )
    throw new Error("Usted no puede hacer modificaciones en este cliente")

  findClientToDelet = await Client.findOneAndDelete({ _id: client_id })
  return client_id
}

const newOrder = async (_, { new_order }, ctx) => {
  const { client_id, order } = new_order

  // verify client exist
  const client = await Client.findById(client_id)
  if (!client) throw new Error("El cliente no existe")

  // verify that the customer belongs to the seller
  if (!ctx.user || client.seller.toString() !== ctx.user.id.toString())
    throw new Error("Usted no puede hacer modificaciones en este cliente")

  // verify stock
  for await (const article of order) {
    const { id, total } = article
    const producto = await Product.findById(id)

    if (total > producto.stock) {
      throw new Error("no hay sufucuiente en stock de " + producto.name)
    } else if (total < producto.stock) {
      // less stock
      producto.stock = producto.stock - total
      await producto.save()
    }
  }

  const newOrder = new Order(new_order)
  // assign seller
  newOrder.seller_id = ctx.user.id

  // save
  const result = await newOrder.save()

  return result
}

const updateOrder = async (_, { id, order: orderInput }, ctx) => {
  const { client_id, order } = orderInput
  const orderToEdit = await Order.findById(id)

  if (!orderToEdit) throw new Error("No existe el cliente a Editar")

  if (!ctx.user || orderToEdit.seller_id.toString() !== ctx.user.id.toString())
    throw new Error("Usted no puede hacer modificaciones en este cliente")

  const clientExist = await Client.findById(client_id)

  if (!clientExist) throw new Error("El cliente no existe")

  if (!ctx.user || clientExist.seller.toString() !== ctx.user.id.toString())
    throw new Error("Usted no puede hacer modificaciones en este cliente")

  // verify stock
  for await (const article of order) {
    const { id, total } = article
    const producto = await Product.findById(id)

    if (total > producto.stock) {
      throw new Error("no hay sufucuiente en stock de " + producto.name)
    } else if (total < producto.stock) {
      // less stock
      producto.stock = producto.stock - total
      await producto.save()
    }
  }

  const orderEdited = await Order.findOneAndUpdate({ _id: id }, orderInput, {
    new: true,
  })

  return {
    oldOrder: orderToEdit,
    editedOrder: orderEdited,
  }
}

const deleteOrder = async (_, { order_id }, ctx) => {
  const order = await Order.findById(order_id)

  if (!order) throw new Error("el pedido no existe")

  if (!ctx.user || order.seller_id.toString() !== ctx.user.id.toString())
    throw new Error("Usted no puede hacer modificaciones en este cliente")

  // delete
  await Order.findOneAndDelete({ _id: order_id })
  return order_id
}

const Mutation = {
  createNewUser,
  createNewProduct,
  updateProduct,
  deleteProduct,
  newClient,
  updateClient,
  deleteClient,
  newOrder,
  updateOrder,
  deleteOrder,
}

module.exports = Mutation
