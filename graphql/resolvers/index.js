const Mutation = require("./mutations")
const Query = require("./queries")

// resolver
const resolvers = { Query, Mutation }

module.exports = resolvers
