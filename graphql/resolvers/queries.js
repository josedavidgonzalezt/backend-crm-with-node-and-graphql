const User = require("../../models/User")
const bcryptjs = require("bcryptjs")
const jwt = require("jsonwebtoken")
const Product = require("../../models/Product")
const Client = require("../../models/Client")
const Order = require("../../models/Order")
const createToken = require("../../token/createToken")

const getAuthenticatedUser = async (_, { token }) => {
  const userID = await jwt.verify(token, process.env.TOKEN_SECRET_WORD)
  return userID
}

const authUser = async (_, { user }) => {
  const { email, password } = user
  const findUser = await User.findOne({ email })

  // si el user existe
  if (!findUser) throw new Error("Este user no existe")

  // revisar password
  const correctPassword = await bcryptjs.compare(password, findUser.password)

  if (!correctPassword) throw new Error("contraseña incorrecta")

  // crear token
  return {
    token: createToken(findUser),
  }
}

const products = async () => {
  try {
    const products = await Product.find({})
    return products
  } catch (error) {
    console.log(error)
  }
}

const product = async (_, { product_id }) => {
  const findProducto = await Product.findById(product_id)
  if (!findProducto) throw new Error("Este Product no existe")

  return findProducto
}

const clients = async () => {
  try {
    const clients = await Client.find({})
    return clients
  } catch (error) {
    console.log(error)
  }
}

const sellerClients = async (_, __, ctx) => {
  try {
    const clients = await Client.find({ seller: ctx.user.id.toString() })
    return clients
  } catch (error) {
    console.log(error)
  }
}

const client = async (_, { client_id }, ctx) => {
  const findClient = await Client.findById(client_id)
  if (!findClient) throw new Error("Cliente no encontrado")
  if (!ctx.user || findClient.seller.toString() !== ctx.user.id.toString())
    throw new Error(
      "No tienes las credenciales necesarias para ver a este cliente"
    )
  return findClient
}

const orders = async () => {
  try {
    const orders = await Order.find({})
    console.log(orders)
    return orders
  } catch (error) {
    console.log(error)
  }
}

const sellerOrders = async (_, __, ctx) => {
  try {
    console.log(ctx)
    const orders = await Order.find({ seller_id: ctx.user.id })
    return orders
  } catch (error) {
    console.log(error)
  }
}

const sellerOrder = async (_, { id }, ctx) => {
  const order = await Order.findById(id)
  console.log(order)
  // order do not exist
  if (!order) throw new Error("orden no encontrada")

  if (!ctx.user || order.seller_id.toString() !== ctx.user.id.toString())
    throw new Error(
      "No tienes las credenciales necesarias para ver esta orden"
    )

  return order
}

const ordersByStatus = async (_, { status }, ctx) => {
  const orders = await Order.find({ status, seller_id: ctx.user.id })
  // order do not exist
  if (!orders) throw new Error("Pedidos no encontrados")
  return orders
}

const bestClients = async () => {
  const clientB = await Order.aggregate([
    // find with status completed
    // like where on SQL
    {  $match: { status: "Completed" } },
    // group by client
    {   
      $group: {  
        _id: "$client_id", 
        total: { $sum: "$amount" }, 
      },
    },

    //  is like join in sql 
    {
      $lookup: {
        from: "clients",
        localField: "_id",
        foreignField: "_id",
        as: "client",
      },
    },
    {
      $sort: { total: -1 },
    },
  ])

  return clientB
}

const Query = {
  authUser,
  getAuthenticatedUser,
  products,
  product,
  clients,
  client,
  sellerClients,
  orders,
  sellerOrders,
  sellerOrder,
  ordersByStatus,
  bestClients,
}

module.exports = Query
