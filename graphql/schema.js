const { gql } = require("apollo-server")

// Schema
const typeDefs = gql`
  type User {
    id: ID
    name: String
    last_name: String
    email: String
    created: String
  }

  type Token {
    token: String!
  }

  type AuthenticatedUser {
    id: ID
    name: String
    last_name: String
    email: String
    exp: String
    iat: String
  }

  input NewUser {
    name: String!
    last_name: String!
    email: String!
    password: String!
  }

  input AuthUser {
    email: String!
    password: String!
  }

  type Product {
    id: ID
    name: String!
    stock: Float!
    price: Float!
    created: String!
  }

  input NewProduct {
    name: String!
    stock: Float!
    price: Float!
  }

  type Client {
    id: ID
    name: String
    last_name: String
    company: String
    email: String
    phone_number: String
    seller: String
  }

  input NewClient {
    name: String!
    last_name: String!
    company: String!
    email: String!
    phone_number: String
  }

  type Client_Updated {
    oldClient: Client
    editedClient: Client
  }

  input ProductOrderInput {
    id: ID
    total: Int
  }

  type ProductOrderType {
    id: ID
    total: Int
  }

  type ProductOrder {
    id: ID
    total: Int
  }

  enum StatusOrder {
    Pending
    Completed
    Cancelled
  }

  input NewOrder {
    order: [ProductOrderInput]!
    amount: Float
    client_id: ID!
    status: StatusOrder
  }

  type Order {
    order: [ProductOrderType]
    amount: Float
    client_id: ID
    seller_id: ID
    created: String
    status: StatusOrder
  }

  type Order_Updated {
    oldOrder: Order
    editedOrder: Order
  }

  type TopCliente {
    total: Float
    client: [Client]
  }
  

  # Queries and mutations #

  type Query {
    # usuarios
    getAuthenticatedUser(token: String!): AuthenticatedUser
    authUser(user: AuthUser!): Token

    #Products
    products: [Product]
    product(product_id: ID!): Product

    #Clients
    clients: [Client]
    sellerClients: [Client]
    client(client_id: ID): Client

    #orders
    orders: [Order]
    sellerOrders: [Order]
    sellerOrder(id: ID): Order
    ordersByStatus(status: String): [Order]

    #searchs
    bestClients: [TopCliente]
  }

  type Mutation {
    # users
    createNewUser(new_user: NewUser!): User

    # products
    createNewProduct(product: NewProduct): Product
    updateProduct(product_id: ID!, product: NewProduct!): Product
    deleteProduct(product_id: ID!): String

    # clients
    newClient(new_client: NewClient!): Client
    updateClient(client_id: ID!, client: NewClient!): Client_Updated
    deleteClient(client_id: ID!): ID

    #Orders
    newOrder(new_order: NewOrder!): Order
    updateOrder(id: ID!, order: NewOrder!): Order_Updated
    deleteOrder(order_id: ID!): ID


  }
`
module.exports = typeDefs
