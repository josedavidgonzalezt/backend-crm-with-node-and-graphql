const jwt = require("jsonwebtoken")

const context = ({ req }) => {
  const token = req.headers["authorization"] || ""
  if (token) {
    try {
      const user = jwt.verify(token, process.env.TOKEN_SECRET_WORD)
      return { user }
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = context
