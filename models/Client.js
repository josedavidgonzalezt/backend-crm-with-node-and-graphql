const mongoose = require("mongoose")

const ClientSchema = mongoose.Schema({
  name: {
    type: String,
    require: true,
    trim: true,
  },
  last_name: {
    type: String,
    require: true,
    trim: true,
  },
  company: {
    type: String,
    require: true,
    trim: true,
  },
  email: {
    type: String,
    require: true,
    trim: true,
    unique: true,
  },
  phone_number: {
    type: String,
    trim: true,
  },
  seller: {
    type: mongoose.Schema.Types.ObjectId,
    require: true,
    trim: true,
    ref: "User",
  },
  created: {
    type: Date,
    default: Date.now(),
  },
})

module.exports = mongoose.model("Client", ClientSchema)
