const mongoose = require("mongoose")

const OrderSchema = mongoose.Schema({
  order: {
    type: Array,
    require: true,
  },
  amount: {
    type: Number,
    require: true,
  },
  client_id: {
    type: mongoose.Schema.Types.ObjectId,
    require: true,
    ref: "Client",
  },
  seller_id: {
    type: mongoose.Schema.Types.ObjectId,
    require: true,
    ref: "User",
  },
  status: {
    type: String,
    default: "Pending",
  },
  created: {
    type: Date,
    default: Date.now(),
  },
})

module.exports = mongoose.model("Order", OrderSchema)
