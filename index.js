const { ApolloServer } = require("apollo-server")
const { resolvers, typeDefs, context } = require("./graphql")
const connectDB = require("./config/mongoDB")

const runServer = async () => {
  //conectar a la base de datos
  connectDB()

  // server
  const server = new ApolloServer({ typeDefs, resolvers, context })
  try {
    const { url } = await server.listen()
    console.log(`🚀 server listening on ${url}`)
  } catch (error) {
    console.log(error)
  }
}

runServer()
