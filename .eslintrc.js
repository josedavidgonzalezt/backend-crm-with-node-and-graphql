module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
  ],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
    semi: ["warn", "never"],
    quotes: ["warn", "double"],
    indent: ["error", 2],
    ["comma-spacing"]: ["error", {
      before: false, 
      after: true }],
    ["comma-dangle"]: ["error", {
      arrays: "always-multiline",
      objects: "always-multiline",
      imports: "always-multiline",
      exports: "always-multiline",
      functions: "never",
    }],
  },
}