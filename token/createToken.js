const jwt = require("jsonwebtoken")
require("dotenv").config({ path: "variables.env" })

const secretWord = process.env.TOKEN_SECRET_WORD
const expiresIn = process.env.EXP_TOKEN

const createToken = (user) => {
  const { email, id, name, last_name } = user
  return jwt.sign({ email, id, name, last_name }, secretWord, { expiresIn })
}

module.exports = createToken
