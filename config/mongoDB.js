const mongoose = require("mongoose")

require("dotenv").config({ path: "variables.env" })

const connectDB = async () => {
  try {
    await mongoose.connect(process.env.DB_MONGO, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })

    console.log("Db conectada")
  } catch (error) {
    console.log(
      "Ha ocurrido un error al conectarse con la base de Datos " + error
    )
    process.exit(1)
  }
}

module.exports = connectDB
